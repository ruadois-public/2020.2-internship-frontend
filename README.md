## Estágio

Bem-vindo(a) ao teste prático para estágio front-end da RuaDois!

### Objetivo

Criar uma aplicação web, em Vue.js, que liste usuários.

### Requisitos

Para este teste, você **deverá** utilizar **Vue.js**.

Dada a seguinte API de usuários: [R2-API](http://5f88a118a8a2b5001641ef57.mockapi.io/ruadois/api/users "R2API") você deverá:

- listá-los por páginas de dez elementos cada, exibindo um card com as informações básicas de um usuário;
- ao selecionar um card, exibir uma página com o perfil e as informações relacionadas ao respectivo usuário;
- deve ser possível cadastrar um usuário, e;
- dentro da página de detalhes, deve ser possível editar ou remover um usuário.

*UI e UX livres*.

> Para paginar a lista de usuários: `?page=x&limit=x`   
> Para ordenar a lista de usuários: `?sortBy=createdAt&order=desc/asc`    
> Para filtrar a lista de usuários (active): `?filter=false`       


### Orientações

1. Criar um fork do repositório https://gitlab.com/ruadois-public/2020.2-internship-frontend.

2. Desenvolver sua solução, e enviar o link do fork com a solução para curriculodev@ruadois.com.br.

3. Enviar junto a solução um video curto de no máximo 5 minutos dando *overview* das suas decisões sobre a solução proposta.

4. Qualquer dúvida ou dificuldade referente a execução do teste, entrar em contato em curriculodev@ruadois.com.br.

5. Você tem até domingo para realizar o teste.

### Pontos importantes:
- Design responsivo;
- Arquitetura de componentes;
- Tratamento de erros;
- Código limpo;
- Pelo menos um teste;

#### Bônus
- Utilizar sass, stylus, less etc;
- Não utilizar libs como Bootstrap, materialize, etc.

Queremos ver seu processo de desenvolvimento, e para isso você pode utilizar o README para inserir outros pontos que considera importantes e que gostaria que avaliássemos com mais cuidado.

Happy hacking! ;)
